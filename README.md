# 2020-06-Grenoble

A CorText Manager distance training session in the framework of the nanocellulose project 

For complementing the RISIS access requested (to Leiden publications DB and RISIS patent DB) by the GAEL laboratory (UMR INRAE, CNRS, UGA, INPG), in the framework of a research project on nanocellulose, the CorText team has provided , in June and July 2020, an advanced training on the use of CorText.
After setting up of the patents and publications corpora – done through the RISIS access - a team of 4 researchers from the Grenoble lab has been trained (at distance) in the use of CorText.
Three dimensions have been explored:
1. Thematic analysis of the structuration of this research field over time, using lexical extraction scripts.
1. Spatial analysis of the locations of knowledge (patents and publications) production at URAs level using the CorText geocoding and geoexploration services
1. Institutional analysis by studying the powerhouse in the field (taking advantage of the harmonisation work carried out at CWTS and at LISIS) and their linkages (through network analysis of collaboration)

This research project - as was done in some previous experiences from the Cortext team (e.g. EMBRIC) - should provide robust knowledge on this booming research field by associating experts in the field of nanocellulose, economists and data scientist.

Beyond the results of this specific project, this cooperation is interesting in the sense as it diffuses the use of CorText beyond the SHS research community, providing a tool for a robust strategic steering of research in nanocellulose.

Antoine Schoen and Patricia Laurens